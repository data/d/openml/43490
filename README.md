# OpenML dataset: BTS-Lyrics

https://www.openml.org/d/43490

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
The dataset was collated as a casual project using data from Genius and Big Hit.
Currently contains 18 albums (check section "Albums in dataset" below for more details).
Columns

id (int) : sequential numerical id to uniquely identify track
album_title (string) : title of the album
eng_ album_title (string) : title of album without non-english characters
album_rd (string) : date that album was released in isoformat (YYYY-MM-DD)
album_seq (int) : sequence of track in album
track_title (string) : title of the track
kor_ track_ title (string) : title of the track with korean characters
eng_ track_ title (string) : title of the track without non-english characters
lyrics (string) : english translated lyrics of the track (from genius)
will be empty for instrumentals
hidden_track (boolean) : indicates whether the track is a hidden track
remix (boolean) : indicates whether the track is a remix
featured (string) : indicates who is featured in the track
performed_by (string) : indicates who performed the track
multiple individuals seperated with ";" (e.g. "RM;SUGA;J-HOPE" would mean that the track is performed by RM, SUGA and J-HOPE)
featured artists will not be reflected in this column
repackaged (boolean) : indicates if the track was already released in an earlier album
language (string) : language of track
KOR: Korean
ENG: English
NA: Not Applicable (e.g. instrumentals)

Other Acknowledgements
Banner and Profile Image
Inspiration
Taylor Swift Song Lyrics from all the albums
Planned Improvements

adding Japanese albums

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43490) of an [OpenML dataset](https://www.openml.org/d/43490). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43490/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43490/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43490/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

